package com.functestcases;


import com.codiotestcore.BaseTest;
import com.codiotestcore.EnvironmentsForTesting;
import com.codiotestcore.Parallelized;
import com.codiotestcore.SubTest;
import com.codiotestcore.subtest.SubtestMethod;
import com.codiotestcore.subtest.SubtestProvider;
import com.codiotestcore.subtest.SubtestRunner;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.List;

@RunWith(Parallelized.class)
public class TestExampleClass extends BaseTest {

    private SubtestRunner runner;

    @Parameterized.Parameters
    public static List getEnvironments() throws Exception {

        List env = EnvironmentsForTesting.getBasicEnvironments();
        return SubtestProvider.getRunners(TestExampleClass.class, env);
    }
    public TestExampleClass(SubtestMethod test, String os, String os_version, String browser, String version) throws Exception {
        super( os, os_version, browser, version );

        runner = test.getRunner( this );
    }

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void runTest() throws Throwable {
        runner.run();
    }



    @SubTest
    public void testCase1() throws Exception {

        signIn(url, login, password);

    }




    @After
    public void tearDown() throws Exception {
        destroy();
    }
}