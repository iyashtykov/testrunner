package com.functestcases;

import com.codiotestcore.BaseTest;
import com.codiotestcore.EnvironmentsForTesting;
import com.codiotestcore.Parallelized;
import com.codiotestcore.SubTest;
import com.codiotestcore.subtest.SubtestMethod;
import com.codiotestcore.subtest.SubtestProvider;
import com.codiotestcore.subtest.SubtestRunner;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@RunWith( Parallelized.class )
public class TestBasicThings extends BaseTest {

    private SubtestRunner runner;

    @Parameterized.Parameters
    public static List getEnvironments() throws Exception {

        List env = EnvironmentsForTesting.getBasicEnvironments();

        return SubtestProvider.getRunners( TestBasicThings.class, env );
    }

    public TestBasicThings(SubtestMethod test, String os, String os_version, String browser, String version) throws Exception {
        super( os, os_version, browser, version );

        runner = test.getRunner( this );
    }

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void runTest() throws Throwable {
        runner.run();
    }


    @SubTest
    public void testExample() throws Exception {


        signIn(url, login, password);

        Assert.assertTrue(getElement("333"));


    }


    @SubTest
    public void testExample2() throws Exception {


        signIn(url, login, password);


    }

    @SubTest
    public void testExample3() throws Exception {


        signIn(url, login, password);


    }

    @SubTest
    public void testExample4() throws Exception {


        signIn(url, login, password);

        Assert.assertTrue(getElement("333"));


    }


    @SubTest
    public void testExample5() throws Exception {


        signIn(url, login, password);


    }

    @SubTest
    public void testExample6() throws Exception {


        signIn(url, login, password);


    }


    @After
    public  void tearDown() throws Exception {
        destroy();
    }


}