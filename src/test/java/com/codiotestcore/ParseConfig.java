package com.codiotestcore;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class ParseConfig {

    private final static String CONFIG_PATH = "config.json";

    public static AppConfig loadAppConfig() {
        AppConfig config = ParseConfig.getObjectForJsonFile(CONFIG_PATH, AppConfig.class);

        String propUrl = System.getProperty( "codioUrl" );
        if ( propUrl != null ) {
            config.getCodioaccount().setUrl( propUrl );
        }
        String propLogin = System.getProperty( "codioLogin" );
        if ( propLogin != null ) {
            config.getCodioaccount().setLogin( propLogin );
        }
        String propPassword = System.getProperty( "codioPassword" );
        if ( propPassword != null ) {
            config.getCodioaccount().setPassword(propPassword );
        }

        return config;
    }


    public static <T> T getObjectForJsonFile(String filename, Class<T> type) {

        T result = null;

        ObjectMapper mapper = new ObjectMapper();
        try {
            result = mapper.readValue(
                    new File(filename),
                    type );
        } catch (JsonParseException e2) {
            // TODO Auto-generated catch block
            System.out.println("ERROR(problems with parsing " + filename + ")");
            e2.printStackTrace();
        } catch (JsonMappingException e2) {
            // TODO Auto-generated catch block
            System.out.println("ERROR(problems with mapping " + filename + ")");
            e2.printStackTrace();
        } catch (IOException e2) {
            // TODO Auto-generated catch block
            System.out.println("ERROR(file " + filename + " not found)");
            e2.printStackTrace();
        }
        return result;
    }

}
