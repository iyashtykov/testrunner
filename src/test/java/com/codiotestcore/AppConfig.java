package com.codiotestcore;


public class AppConfig {
    private BrowserStackSettings browserstack;


    public BrowserStackSettings getBrowserstack() {
        return browserstack;
    }

    public void setBrowserstack(BrowserStackSettings browserstack) {
        this.browserstack = browserstack;
    }

    public static class BrowserStackSettings {
        private String login;
        private String key;
        private int sessioncount;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public int getSessioncount() {
            return sessioncount;
        }

        public void setSessioncount(int sessioncount) {
            this.sessioncount = sessioncount;
        }
    }



    private CodioAccountSettings codioaccount;
    public CodioAccountSettings getCodioaccount(){
        return codioaccount;
    }
    public void setCodioaccount(CodioAccountSettings codioaccount){
        this.codioaccount = codioaccount;
    }

    public static class CodioAccountSettings{
        private String url;
        private String login;
        private String password;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }


}
