package com.codiotestcore;


import org.openqa.selenium.Platform;

import java.util.LinkedList;

public class EnvironmentsForTesting {

    public static LinkedList getAllEnvironments() throws Exception {
        LinkedList<String[]> env = new LinkedList();

    //    env.addAll(getWindowsXPEnvironments());
        env.addAll(getWindows7Environments());
    //    env.addAll(getWindows8Environments());
    //    env.addAll(getWindows8_1Environments());
        env.addAll(getMACEnvironments());

        System.out.println("Count of browser versions: " + env.size());
        return env;
    }

    public static LinkedList getBasicEnvironments() throws Exception {
        LinkedList<String[]> env = new LinkedList();


//        env.add(new String[]{"Windows", "7","IE","10.0"});
        env.add(new String[]{"Windows", "7","chrome","45"});
        env.add(new String[]{"Windows", "7","firefox","41.0"});
    /*    env.add(new String[]{"Windows", "XP","chrome","29"});
        env.add(new String[]{"Windows", "8","chrome","29"});
        env.add(new String[]{"Windows", "8.1","chrome","29"});

        env.add(new String[]{"Windows", "7","firefox","24.0"});
        env.add(new String[]{"Windows", "XP","firefox","24.0"});
        env.add(new String[]{"Windows", "8","firefox","24.0"});
        env.add(new String[]{"Windows", "8.1","firefox","24.0"});

        env.add(new String[]{"Windows", "7","IE","10.0"});
        env.add(new String[]{"Windows", "8","IE","10.0"});


        env.add(new String[]{"OS X", "Snow Leopard","chrome","29"});
        env.add(new String[]{"OS X", "Snow Leopard","firefox","24.0"});
        env.add(new String[]{"OS X", "Lion","chrome","29"});
        env.add(new String[]{"OS X", "Lion","firefox","24.0"});
        env.add(new String[]{"OS X", "Mountain Lion","chrome","29"});
        env.add(new String[]{"OS X", "Mountain Lion","firefox","24.0"});

        env.add(new String[]{"OS X", "Lion","safari","6.0"});
        env.add(new String[]{"OS X", "Mountain Lion","safari","6.0"});
    */

        return env;
    }



    //   Environments by browsers

    public static LinkedList getChromeEnvironments() throws Exception {
        LinkedList<String[]> env = new LinkedList();

        //  windows xp/7 and MAC
        for(int i = 21; i < 30; i++){
            env.add(new String[]{"Windows", "XP","chrome","" + i });
            env.add(new String[]{"Windows", "7","chrome","" + i });
            env.add(new String[]{"OS X", "Snow Leopard","chrome","" + i });
            env.add(new String[]{"OS X", "Lion","chrome","" + i });
            env.add(new String[]{"OS X", "Mountain Lion","chrome","" + i });
        }

        //  windows 8 and 8.1
        for(int i = 22; i < 30; i++){
            env.add(new String[]{Platform.WIN8.toString(), "chrome", "" + i});
        }

        return env;
    }

    public static LinkedList getFirefoxEnvironments() throws Exception {
        LinkedList<String[]> env = new LinkedList();

        for(int i = 22; i < 25; i++){
            env.add(new String[]{"Windows", "XP","firefox","" + i + ".0"});
            env.add(new String[]{"Windows", "7","firefox","" + i + ".0"});
            env.add(new String[]{"Windows", "8","firefox","" + i + ".0"});
            env.add(new String[]{"Windows", "8.1","firefox","" + i + ".0"});
            env.add(new String[]{"OS X", "Snow Leopard","firefox","" + i + ".0"});
            env.add(new String[]{"OS X", "Lion","firefox","" + i + ".0"});
            env.add(new String[]{"OS X", "Mountain Lion","firefox","" + i + ".0"});
        }
        return env;
    }

    public static LinkedList getIEEnvironments() throws Exception {
        LinkedList<String[]> env = new LinkedList();

        env.add(new String[]{"Windows", "7","IE","10.0"});
        env.add(new String[]{"Windows", "8","IE","10.0"});

        return env;
    }

    public static LinkedList getSafariEnvironments() throws Exception {
        LinkedList<String[]> env = new LinkedList();

        env.add(new String[]{"OS X", "Lion","safari","6.0"});
        env.add(new String[]{"OS X", "Mountain Lion","safari","6.0"});

        return env;
    }




    // Environments by OS

    public static LinkedList getWindowsXPEnvironments() throws Exception {
        LinkedList<String[]> env = new LinkedList();

        for(int i = 21; i < 30; i++){
            env.add(new String[]{"Windows", "XP","chrome","" + i});
        }
        for(int i = 22; i < 25; i++){
            env.add(new String[]{"Windows", "XP","firefox","" + i + ".0"});
        }


        return env;
    }

    public static LinkedList getWindows7Environments() throws Exception {
        LinkedList<String[]> env = new LinkedList();

        for(int i = 21; i < 30; i++){
            env.add(new String[]{"Windows", "7","chrome","" + i});
        }
        for(int i = 22; i < 25; i++){
            env.add(new String[]{"Windows", "7","firefox","" + i + ".0"});
        }

        env.add(new String[]{"Windows", "7","IE","10.0"});

        return env;
    }

    public static LinkedList getWindows8Environments() throws Exception {
        LinkedList<String[]> env = new LinkedList();

        for(int i = 22; i < 30; i++){
            env.add(new String[]{"Windows", "8","chrome","" + i});
        }
        for(int i = 22; i < 25; i++){
            env.add(new String[]{"Windows", "8","firefox","" + i + ".0"});
        }

        env.add(new String[]{"Windows", "8","IE","10.0"});

        return env;
    }


    public static LinkedList getWindows8_1Environments() throws Exception {
        LinkedList<String[]> env = new LinkedList();

        for(int i = 22; i < 30; i++){
            env.add(new String[]{"Windows", "8.1","chrome","" + i});
        }
        for(int i = 22; i < 25; i++){
            env.add(new String[]{"Windows", "8.1","firefox","" + i + ".0"});
        }


        return env;
    }

    public static LinkedList getMACEnvironments() throws Exception {
        LinkedList<String[]> env = new LinkedList();

        for(int i = 21; i < 30; i++){
            env.add(new String[]{"OS X", "Snow Leopard","chrome","29"});
            env.add(new String[]{"OS X", "Lion","chrome","29"});
            env.add(new String[]{"OS X", "Mountain Lion","chrome","29"});
        }
        for(int i = 22; i < 25; i++){
            env.add(new String[]{"OS X", "Snow Leopard","firefox","" + i + ".0"});
            env.add(new String[]{"OS X", "Lion","firefox","" + i + ".0"});
            env.add(new String[]{"OS X", "Mountain Lion","firefox","" + i + ".0"});
        }

        env.add(new String[]{"OS X", "Lion","safari","6.0"});
        env.add(new String[]{"OS X", "Mountain Lion","safari","6.0"});

        return env;
    }

}
