package com.codiotestcore.subtest;

import com.codiotestcore.SubTest;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 16.10.13
 * Time: 18:23
 * To change this template use File | Settings | File Templates.
 */
public class SubtestProvider {
    public static <T> List<SubtestMethod[]> getRunners( Class<T> clazz ) {
        List<SubtestMethod[]> result = new ArrayList<SubtestMethod[]>();
        for ( Method m : clazz.getMethods() ) {
            if ( m.isAnnotationPresent(SubTest.class) ) {
                SubtestMethod subtest = new SubtestMethod( m );
                result.add( new SubtestMethod[] { subtest } );
            }
        }
        return result;
    }

    public static <T> List<Object[]> getRunners( Class<T> clazz, List environments ) {
        List<Object[]> result = new ArrayList<Object[]>();
        for ( Method m : clazz.getMethods() ) {
            if ( m.isAnnotationPresent(SubTest.class) ) {
                SubtestMethod subtest = new SubtestMethod( m );

                for ( Object envParams : environments ) {
                    List<Object> params = new ArrayList<Object>();
                    params.add( subtest );
                    params.addAll( Arrays.asList( (String[]) envParams ) );

                    result.add( params.toArray() );
                }

            }
        }
        return result;
    }
}
