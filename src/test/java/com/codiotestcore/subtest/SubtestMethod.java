package com.codiotestcore.subtest;

import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 16.10.13
 * Time: 18:25
 * To change this template use File | Settings | File Templates.
 */
public class SubtestMethod {
    private Method method;

    public SubtestMethod(Method param) {
        method = param;
    }

    public SubtestRunner getRunner( Object instance ) {
        return new SubtestRunner( instance, method );
    }
}
