package com.codiotestcore.subtest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 16.10.13
 * Time: 18:19
 * To change this template use File | Settings | File Templates.
 */
public class SubtestRunner {
    private Method method;
    private Object instance;

    public SubtestRunner(Object instance, Method method) {
        this.instance = instance;
        this.method = method;
    }

    public void run() throws Throwable {
        try {
            method.invoke( instance );
        }
        catch ( InvocationTargetException e ) {
            throw e.getCause();
        }
        catch ( IllegalAccessError e ) {
            throw new RuntimeException( e );
        }
    }
}
