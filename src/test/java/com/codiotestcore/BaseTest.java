package com.codiotestcore;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class BaseTest {

    private String platform;
    private String os;
    private String os_version;
    private String browserName;
    private String browserVersion;

    private WebDriver driver;
    AppConfig config = loadConfig();
    public String url = config.getCodioaccount().getUrl();
    public String login = config.getCodioaccount().getLogin();
    public String password = config.getCodioaccount().getPassword();

    public BaseTest() throws Exception {
        driver = loadDriver( "Windows", "7", "chrome","31" );
    }

    public BaseTest(String os, String os_version, String browserName, String browserVersion) throws Exception {
        this.os = os;
        this.os_version = os_version;
        this.browserName = browserName;
        this.browserVersion = browserVersion;
        driver = loadDriver( os, os_version, browserName, browserVersion );

        // set screen size for browser window
        driver.manage().window().maximize();

    }

    protected AppConfig loadConfig() {
        return ParseConfig.loadAppConfig();
    }

    protected WebDriver loadDriver( String os, String os_version, String browserName, String browserVersion ) throws Exception {
        AppConfig config = loadConfig();

        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("os", os);
        capability.setCapability("os_version", os_version);
        capability.setCapability("browser", browserName);
        capability.setCapability("browserVersion", browserVersion);
        capability.setCapability("build", "started on " + getDate());
        capability.setCapability("acceptSslCerts", true);
        capability.setJavascriptEnabled(true);
    //    capability.setCapability("browserstack.debug", true);

        boolean remote = false;

        if(remote){
            RemoteWebDriver remoteWebDriver = new RemoteWebDriver(
                    new URL("http://" + config.getBrowserstack().getLogin() + ":" + config.getBrowserstack().getKey() + "@hub.browserstack.com/wd/hub"),
                    capability
            );
            remoteWebDriver.setFileDetector(new LocalFileDetector());
            return remoteWebDriver;
        }
        else{
            if(browserName.equals("chrome")){
                System.setProperty("webdriver.chrome.driver", ".\\chromedriver.exe");
                return new ChromeDriver();
            }
            else if(browserName.equals("firefox")){
                return new FirefoxDriver();
            }
            else if(browserName.equals("IE")){
               System.setProperty("webdriver.ie.driver", ".\\IEDriverServer.exe");
               return new InternetExplorerDriver();
            }
            else {
                System.setProperty("webdriver.chrome.driver", ".\\chromedriver.exe");
                return new ChromeDriver();
            }

        }
    }


    // User actions

    public String getDate(){
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm");
        Date date = new Date();
        return dateFormat.format(date);
    }


    public void signIn(String url, String login, String password){

        driver.get(url + "/#/login");
        sleep(2000);
        wait(60);

        WebElement username_field = driver.findElement(By.xpath("//input[@name='loginEmail']"));
        WebElement password_field = driver.findElement(By.xpath("//input[@name='loginPass']"));
        WebElement sign_in_button = driver.findElement(By.xpath("//button[contains(string(),'Login')]"));
        wait(30);
        username_field.clear();
        username_field.sendKeys(login);
        password_field.clear();
        password_field.sendKeys(password);
        sign_in_button.click();
    //    wait(60);
    //    driver.findElement(By.xpath("//nav[@class='navigation']"));


    }

    public boolean getElement(String str){

        if(str.equals("123"))
            return false;
        else
            return true;
    }



    public String getID(){
        return UUID.randomUUID().toString();
    }

    public void wait(int time){
        driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
    }
    public void sleep(int milisecond){
        try {
            Thread.sleep(milisecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    protected void destroy() {
        driver.quit();
    }

    protected WebDriver driver() {
        return driver;
    }
}
