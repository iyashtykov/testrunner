
## Quick start

Choose one of the following options:

1. Clone the git repo — git clone https://github.com/codio/autotests.git

2. Install java [jdk](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

3. Install [maven](http://maven.apache.org/download.cgi)

4. Change file config.json and put your browserstack login and key

5. For running tests go to project's root and on command line run 'mvn test' or import project as maven module to IDE and run as junit tests

6. Results of tests see on [browserstack](https://www.browserstack.com/automate)

Example with running junit test from IDE = http://screencast.com/t/1X04MJOwvsn

Example command for running junit test from command line: "mvn -DtestFile=com/functestcases/TestCreateProject test" - http://screencast.com/t/ieHDOe03PJLU
